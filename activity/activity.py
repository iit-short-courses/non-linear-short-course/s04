import pandas as pd

artist_band = {
        "band_name": "BTS",
        "years_active": 10,
        "hit_songs": ["Dynamite", "Mic Drop", "Fire"],
        "is_active": True
}

artist = {
    "artist_name": "Suga",
    "company": "Big Hit Entertainment",
    "age": 30,
    "artist_band": artist_band
}

artist_copy = artist.copy()

artist_copy.update(artist_band)
print(artist_copy)
print('')

# ------------------------------------------------------------------------------------

student_info = {
    "class": {
        "student": {
            "name": "Joon",
            "marks": {
                "physics": 85,
                "history": 90
            }
        }
    }
}

print(f"History Grade: {student_info['class']['student']['marks']['history']}")
print('')

# ------------------------------------------------------------------------------------

personal_info = {
    "name": "Jean",
    "age": 31,
    "salary": 45000,
    "city": "Seoul"
}
# keys to extract
keys = ["name", "salary"]

name_salary = dict(
    {
        "name": personal_info["name"],
        "age": personal_info["age"]
    }
)
print(name_salary)
print('')

# ----------------------------------------------------------------------------------

employees = {
    "emp1": {"full_name": "Amy Santiago", "salary": 45000},
    "emp2": {"full_name": "Charles Boyle", "salary": 50000},
    "emp3": {"full_name": "Rosa Diaz", "salary": 40000},
    "emp4": {"full_name": "Jake Peralta", "salary": 45000},
}

employees["emp4"]["salary"] = 55000
print(employees)
print('')

# ---------------------------------------------------------------------------------

exam_data = {
    'name': ['Dwilight', 'Michael', 'Jim', 'Pam', 'Andy'],
    'score': [12.5, 5, 10, 16.5, 9],
    'attempts': [1, 3, 2, 1, 3],
    'qualify': ['yes', 'no', 'yes', 'yes', 'no']
}

df = pd.DataFrame(exam_data, index = ['a','b','c','d','e'])
print(df)
print('')
print(df.loc['c'])