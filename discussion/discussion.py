# Hashtables
# Review the concept of Dictionary
# Apply the method of Hash Table
# Create a Data Frame

# 1. Dictionaries and its Methods
# 2. Hash Tables
# 3. Data Frame

# Dictionary ---> key value pairs, where value can be lists or another dictionary too.

# Hash Table ----> 
# Hash table are a type of data structure in which the address or the index value of the data element is generated from a hash function
# Accessing data is faster as the index value behaves as a key for the data value

# Dictionary data types represent the implementation of hash tables
# Keys in the dictionary satisfy the following requirements:
# 1. The keys of the dictionary are hashable

# Data Frame is a module from pandas file.
# it is a 2D structure, aligned in a tabular fashion in rows and columns

# --------------------------------------------------------
# mini activity
# create a dictionary for an employee containing its name, level, and if they are regular

employee_info = {
    "name": "Rakshan Tej",
    "level": "Senior Developer",
    "is_regular": True
}

information = {
    "name": "Komal Meena",
    "Age": 68,
    "is_a_youtuber": False
}

print(information)
print('')

# using dict function

company_info = dict(
    {
        "Company Name": "Aksh Industries",
        "Nature": "Software/ IT",
        "is_big_company": True
    }
)

print(company_info)
print('')

# Creating nested dictionaries

companies = {
    "Company 1": company_info,
    "Company 2": {
        "Company Name": "Aaniya Industries",
        "Nature": "Business/ Marketing",
        "is_big_company": True
    },
    "Company 3": {
        "Company Name": "Aaniya and Aksh Infotainment",
        "Nature": "Music",
        "is_big_company": True
    }
}
print(companies)
print('')

# Review: Dictionary Methods

# clear() - clears out the whole dictionary
information.clear()
print(information)
print('')

# copy() - returns a copy of the dictionary
copy_employee_info = employee_info.copy()
print(copy_employee_info)
print('')

# difference between copy() and =
example_1 = {
    "id": 1,
    "username": "thethinker"
}
copy_1 = example_1.copy()
copy_1.clear()
print(f"copy method: {copy_1}")
print(f"1: {example_1}")

example_2 = {
    "id": 2,
    "username": "Fibonacci_me"
}
copy_2 = example_2
copy_2.clear()
print(f"Assignment Operator: {copy_2}")
print(f"2: {example_2}")
print('')

# get() - returns the value for the specified key if the key is in the dictionary
print(copy_employee_info.get("name"))
print('')

# items() - returns a view object that displays a list of dictionary's tuple pairs
print(copy_employee_info.items())
print('')

# keys() - extracts the keys of the dictionary and returns the list of keys as a view object.
print(copy_employee_info.keys())
print('')

# values() - extracts the values of the dictionary and returns the list of values as a view object.
print(copy_employee_info.values())
print('')

# pop(key) - removes and returns an element from a dictionary having the given key
print(copy_employee_info.pop("name"))
print('')

# popitem() - removes and returns the last element pair inserted into the dictionary
copy_employee_info.popitem()
print(copy_employee_info)
print('')

# update() - updates the dictionary with elements from a dictionary objects
copy_employee_info.update(company_info)
print(copy_employee_info)
print('')

# values() - returns a view object that displays a list of all the values in the dictionary
print(copy_employee_info.values())
print('')

# Hash Table

# Accessing values
grocery_prices = {
    "Onion": 600,
    "Chicken": 55,
    "Banana": 90,
    "Tomato": 40
}

print(grocery_prices['Banana'])
print('')

# Accessing values using for loop
for item in grocery_prices.items():
    # print(item[0])
    # print(item[1])
    print('Item:{name}, price:{price}'.format(name = item[0],price = item[1]))

# Updating values
grocery_prices["Onion"] = 20
grocery_prices.pop("Tomato")
grocery_prices.popitem()
print('')
print(grocery_prices)
print('')

# Data Frame
import pandas as pd

game_stats = {
    "player_1": [72, 2, 9, 5],
    "player_2": [65, 4, 12, 7]
}

df = pd.DataFrame(game_stats)
print(df)
print('')

# iloc[] - locates the specific row
print("Value of row 1")
print(df.iloc[1])
print('')

# naming your own indexes
df = pd.DataFrame(game_stats, index = ["Level", "K", "D", "A"])
print(df)
print('')

# locates and returns the specific row using name index
print(df.loc["Level"])
print('')


# Implementation of DataFram with CSV data
df = pd.read_csv("./session_4.csv")
print(df)
